﻿CREATE TABLE [dbo].[DVDkolcsonzo] (
    [id]             INT          NOT NULL identity(1,1),
    [Film_ID]        INT          NOT NULL,
    [Tipus]          VARCHAR (30) NULL,
    [Ar]             INT          NULL,
    [Duplaretegu]    BIT          NULL,
    [Gyujtoi_kiadas] BIT          NULL,
    [Regio_kod]      INT          NULL,
    
);

CREATE TABLE [dbo].[Film] (
    [DVDkolcsonzo_id]  INT          NOT NULL,
    [Film_ID]          INT          NOT NULL identity(1,1),
    [Cim]              VARCHAR (40) NULL,
    [Rendezo]          VARCHAR (40) NULL,
    [Film_Studio]      VARCHAR (40) NULL,
    [Megjelenes_ideje] VARCHAR (20) NULL,
    [Bevetel]          INT          NULL,
);

CREATE TABLE [dbo].[Ugyfel] (
    [Ugyfel_ID] INT          NOT NULL identity(1,1),
    [Nev]          VARCHAR (40) NULL,
    [Cim]          VARCHAR (40) NULL,
    [E_mail]       VARCHAR (40) NULL,
    [Eletkor]      INT          NULL,
    [Telefon]      VARCHAR (40) NULL,
);

CREATE TABLE [dbo].[Kolcsonzes] (
    [id]           INT NOT NULL identity(1,1),
    [Film_ID]      INT NULL,
    [Ugyfel_ID] INT NULL,
);


insert into DVDkolcsonzo (Film_ID,Tipus,Ar,Duplaretegu,Gyujtoi_kiadas,Regio_kod)
values (
1,
'HD',
1500,
0,
0,
8
);

insert into DVDkolcsonzo (Film_ID,Tipus,Ar,Duplaretegu,Gyujtoi_kiadas,Regio_kod)
values (
2,
'FullHD',
3000,
1,
0,
6
);

insert into DVDkolcsonzo (Film_ID,Tipus,Ar,Duplaretegu,Gyujtoi_kiadas,Regio_kod)
values (
3,
'4k',
6000,
1,
1,
8
);

insert into Film (DVDkolcsonzo_id,Cim,Rendezo,Film_Studio,Megjelenes_ideje,Bevetel)
values
(
1,
'Sötét Lovag',
'Christopher Nolan',
'Warner Bros. Pictures',
'2008.08.07',
1001921825
);

insert into Film (DVDkolcsonzo_id,Cim,Rendezo,Film_Studio,Megjelenes_ideje,Bevetel)
values
(
2,
'Schindler Listája',
'Steven Spielberg',
'Universal Pictures',
'1994.03.10',
321306305
);

insert into Film (DVDkolcsonzo_id,Cim,Rendezo,Film_Studio,Megjelenes_ideje,Bevetel)
values
(
3,
'A remény rabjai',
'Frank Darabont',
'Castle Rock Entertainment',
'1995.05.25',
59800000
);

insert into Ugyfel (Nev,Cim,E_mail,Eletkor,Telefon)
values
(
'Nagy Róbert',
'Budapest, Hős utca 2',
'nagyrobert@gmail.com',
25,
06205246742
);

insert into Ugyfel (Nev,Cim,E_mail,Eletkor,Telefon)
values
(
'Kalányos Szilveszter',
'Budapest, Zátony utca 16',
'kalanyosszilveszter@gmail.com',
30,
06704524674
);

insert into Ugyfel (Nev,Cim,E_mail,Eletkor,Telefon)
values
(
'Oláh Otília',
'Budapest, Tarjáni út 25',
'olahotilia@gmail.com',
18,
063008242142
);

insert into Kolcsonzes(Film_ID,Ugyfel_ID)
values
(
1,
1
);

insert into Kolcsonzes(Film_ID,Ugyfel_ID)
values
(
2,
2
);

insert into Kolcsonzes(Film_ID,Ugyfel_ID)
values
(
3,
3
);

alter table Film add CONSTRAINT [Film_pk1] PRIMARY KEY CLUSTERED ([Film_ID] ASC);
alter table DVDKolcsonzo add CONSTRAINT [DVDKolcsonzo_pk1] PRIMARY KEY CLUSTERED ([id] ASC);
alter table Ugyfel add CONSTRAINT [Ugyfel_pk] PRIMARY KEY CLUSTERED ([Ugyfel_ID] ASC);
alter table Kolcsonzes add CONSTRAINT [Kolcsonzes_fk1] PRIMARY KEY CLUSTERED ([id] ASC);

alter table Film add CONSTRAINT [Film_fk1] FOREIGN KEY ([DVDkolcsonzo_id]) REFERENCES [dbo].[DVDkolcsonzo] ([id]);
alter table DVDkolcsonzo add CONSTRAINT [DVDkolcsonzo_fk1] FOREIGN KEY ([Film_ID]) REFERENCES [dbo].[Film] ([Film_id]);


alter table Kolcsonzes add CONSTRAINT [Kolcsonzes_fk3] FOREIGN KEY ([Ugyfel_ID]) REFERENCES [dbo].[Ugyfel] ([Ugyfel_ID]);
alter table Kolcsonzes add CONSTRAINT [Kolcsonzes_fk2] FOREIGN KEY ([Film_ID]) REFERENCES [dbo].[Film] ([Film_ID]);



