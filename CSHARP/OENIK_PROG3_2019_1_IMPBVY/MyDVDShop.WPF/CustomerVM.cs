﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyDVDShop.WPF
{
    class CustomerVM : ObservableObject
    {
        private int customerId;
        private string name;
        private string address;
        private string email;
        private int age;
        private string mobile;

        public string Mobile
        {
            get { return mobile; }
            set { Set(ref mobile, value); }
        }


        public int Age
        {
            get { return age; }
            set { Set(ref age, value); }
        }

        public string E_mail
        {
            get { return email; }
            set { Set(ref email, value); }
        }


        public string Address
        {
            get { return address; }
            set { Set(ref address, value); }
        }


        public string Name
        {
            get { return name; }
            set { Set(ref name,value); }
        }


        public int Customer_ID
        {
            get { return customerId; }
            set { Set(ref customerId, value); }
        }

        public void CopyFrom(CustomerVM other)
        {
            if (other == null) return;
            this.Customer_ID = other.Customer_ID;
            this.Name = other.Name;
            this.Address = other.Address;
            this.E_mail = other.E_mail;
            this.Age = other.Age;
            this.Mobile = other.Mobile;
        }

    }
}
