﻿// <copyright file="Test.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyDVDShop.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using MyDVDShop.Data;
    using MyDVDShop.Logic;
    using MyDVDShop.Repository;
    using NUnit.Framework;

    /// <summary>
    /// Test class with mockedrepository
    /// </summary>
    [TestFixture]
    public class Test
    {
        private Mock<IRepository<Ugyfel>> mockedRepo;
        private Mock<IRepository<DVDkolcsonzo>> mockedDVD;
        private Mock<IRepository<Film>> mockedFilm;
        private Mock<IRepository<Kolcsonze>> mockedKolcsonzes;

        private ILogic<DVDkolcsonzo> dvdlogic;
        private ILogic<Kolcsonze> kolcsonzeslogic;
        private ILogic<Ugyfel> ugyfellogic;
        private ILogic<Film> filmlogic;

        /// <summary>
        /// This is the setup method for the testes, it has mockedrepositories and logic declarations
        /// </summary>
        [OneTimeSetUp]

        public void Setup()
        {
            this.mockedRepo = new Mock<IRepository<Ugyfel>>();
            this.mockedRepo.Setup(t => t.Insert(It.IsAny<Ugyfel>())).Returns(true);
            this.mockedRepo.Setup(t => t.Delete(It.IsAny<Ugyfel>())).Returns(true);
            this.mockedRepo.Setup(t => t.Update(It.IsAny<Ugyfel>())).Returns(true);

            this.mockedDVD = new Mock<IRepository<DVDkolcsonzo>>();
            this.mockedDVD.Setup(t => t.Insert(It.IsAny<DVDkolcsonzo>())).Returns(true);
            this.mockedDVD.Setup(t => t.Delete(It.IsAny<DVDkolcsonzo>())).Returns(true);
            this.mockedDVD.Setup(t => t.Update(It.IsAny<DVDkolcsonzo>())).Returns(true);

            this.mockedFilm = new Mock<IRepository<Film>>();
            this.mockedFilm.Setup(t => t.Insert(It.IsAny<Film>())).Returns(true);
            this.mockedFilm.Setup(t => t.Delete(It.IsAny<Film>())).Returns(true);
            this.mockedFilm.Setup(t => t.Update(It.IsAny<Film>())).Returns(true);

            this.mockedKolcsonzes = new Mock<IRepository<Kolcsonze>>();
            this.mockedKolcsonzes.Setup(t => t.Insert(It.IsAny<Kolcsonze>())).Returns(true);
            this.mockedKolcsonzes.Setup(t => t.Delete(It.IsAny<Kolcsonze>())).Returns(true);
            this.mockedKolcsonzes.Setup(t => t.Update(It.IsAny<Kolcsonze>())).Returns(true);
            IList<Ugyfel> ugyfelek = new List<Ugyfel>
            {
                new Ugyfel()
                {
                    Ugyfel_ID = 1,
                    Nev = "Kalányos Szilveszter",
                    Cim = "Budapest, Tarjáni út 10",
                    E_mail = "kalanyos@szilveszter.hu",
                    Eletkor = 18,
                    Telefon = "+36709496011"
                },

                new Ugyfel()
                {
                    Ugyfel_ID = 2,
                    Nev = "Kalányos Eszter",
                    Cim = "Szeged, Tarjáni út 10",
                    E_mail = "kalanyos@eszter.hu",
                    Eletkor = 20,
                    Telefon = "+36309496011"
                }
            };
            IList<DVDkolcsonzo> dVDkolcsonzo = new List<DVDkolcsonzo>()
            {
                new DVDkolcsonzo()
                {
                    Film_ID = 1,
                    id = 1,
                    Ar = 3000,
                    Duplaretegu = false,
                    Gyujtoi_kiadas = true,
                    Regio_kod = 6,
                    Tipus = "fullhd"
                },

                   new DVDkolcsonzo()
                    {
                        Film_ID = 2,
                        id = 2,
                        Ar = 4000,
                        Duplaretegu = true,
                        Gyujtoi_kiadas = false,
                        Regio_kod = 7,
                        Tipus = "hdready"
                    }
            };

            IList<Film> films = new List<Film>()
                {
                new Film()
                {
                   DVDkolcsonzo_id = 1,
                   Film_ID = 1,
                   Cim = "Remény rabjai",
                   Rendezo = "Kalányos Rómeó",
                   Film_Studio = "Dikh TV",
                   Megjelenes_ideje = "2019.04.20",
                   Bevetel = 5213
                },

                   new Film()
                    {
                   DVDkolcsonzo_id = 2,
                   Film_ID = 2,
                   Cim = "Legenda vagyok",
                   Rendezo = "Christopher Nolan",
                   Film_Studio = "HBO",
                   Megjelenes_ideje = "2016.04.20",
                   Bevetel = 521310
                    }
            };

            IList<Kolcsonze> kolcsonzes = new List<Kolcsonze>()
                {
                new Kolcsonze()
                {
                   id = 1,
                   Film_ID = 1,
                   Ugyfel_ID = 1
                },

                   new Kolcsonze()
                    {
                        id = 2,
                        Film_ID = 2,
                        Ugyfel_ID = 2
                    }
            };

            this.mockedRepo.Setup(t => t.GetAll()).Returns(ugyfelek);
            this.ugyfellogic = new Logic<Ugyfel>(this.mockedRepo.Object);

            this.mockedDVD.Setup(t => t.GetAll()).Returns(dVDkolcsonzo);
            this.dvdlogic = new Logic<DVDkolcsonzo>(this.mockedDVD.Object);

            this.mockedFilm.Setup(t => t.GetAll()).Returns(films);
            this.filmlogic = new Logic<Film>(this.mockedFilm.Object);

            this.mockedKolcsonzes.Setup(t => t.GetAll()).Returns(kolcsonzes);
            this.kolcsonzeslogic = new Logic<Kolcsonze>(this.mockedKolcsonzes.Object);
        }

        /// <summary>
        /// Ugyfel delete test
        /// </summary>
        [Test]
        public void GivenUgyfel_WhenDelete_ReturnsTrue()
        {
            Ugyfel u = new Ugyfel()
            {
                Ugyfel_ID = 1,
                Nev = "Kalányos Szilveszter",
                Cim = "Budapest, Tarjáni út 10",
                E_mail = "kalanyos@szilveszter.hu",
                Eletkor = 24,
                Telefon = "+36709496011"
            };

            bool result = this.ugyfellogic.Delete(u);
            Assert.IsTrue(result);
        }

        /// <summary>
        /// Ugyfel insert test
        /// </summary>
        [Test]
        public void GivennewUgyfel_WhenInsert_ReturnsTrue()
        {
            Ugyfel u = new Ugyfel()
            {
                Ugyfel_ID = 1,
                Nev = "Kalányos Szilveszter",
                Cim = "Budapest, Tarjáni út 10",
                E_mail = "kalanyos@szilveszter.hu",
                Eletkor = 24,
                Telefon = "+36709496011"
            };

            bool result = this.ugyfellogic.Insert(u);
            Assert.IsTrue(result);
        }

        /// <summary>
        /// Ugyfel update test
        /// </summary>
        [Test]
        public void GivenUgyfel_WhenUpdate_ReturnsTrue()
        {
            Ugyfel u = new Ugyfel()
            {
                Ugyfel_ID = 1,
                Nev = "Kalányos Szilveszter",
                Cim = "Budapest, Tarjáni út 10",
                E_mail = "kalanyos@szilveszter.hu",
                Eletkor = 24,
                Telefon = "+36709496011"
            };

            bool result = this.ugyfellogic.Update(u);
            Assert.IsTrue(result);
        }

        /// <summary>
        /// Get every Ugyfel test
        /// </summary>
        [Test]
        public void WhenGetAllUgyfel_ThenReturnsList()
        {
            var result = this.ugyfellogic.GetAll();

            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count());
        }

        /// <summary>
        /// Film delete test
        /// </summary>
        [Test]
        public void GivenFilm_WhenDelete_ReturnsTrue()
        {
            Film f = new Film()
            {
                   DVDkolcsonzo_id = 4,
                   Film_ID = 4,
                   Cim = "A diktátor",
                   Rendezo = "Kalányos József",
                   Film_Studio = "Dikh TV",
                   Megjelenes_ideje = "2019.04.20",
                   Bevetel = 5213
            };

            bool result = this.filmlogic.Delete(f);
            Assert.IsTrue(result);
        }

        /// <summary>
        /// Film insert test
        /// </summary>
        [Test]
        public void GivennewFilm_WhenInsert_ReturnsTrue()
        {
            Film f = new Film()
            {
                DVDkolcsonzo_id = 4,
                Film_ID = 4,
                Cim = "A diktátor",
                Rendezo = "Kalányos József",
                Film_Studio = "Dikh TV",
                Megjelenes_ideje = "2019.04.20",
                Bevetel = 5213
            };

            bool result = this.filmlogic.Insert(f);
            Assert.IsTrue(result);
        }

        /// <summary>
        /// Film update test
        /// </summary>
        [Test]
        public void GivenFilm_WhenUpdate_ReturnsTrue()
        {
            Film f = new Film()
            {
                DVDkolcsonzo_id = 4,
                Film_ID = 4,
                Cim = "A diktátor",
                Rendezo = "Kalányos József",
                Film_Studio = "Dikh TV",
                Megjelenes_ideje = "2019.04.20",
                Bevetel = 5213
            };

            bool result = this.filmlogic.Update(f);
            Assert.IsTrue(result);
        }

        /// <summary>
        /// Get all Films test
        /// </summary>
        [Test]
        public void WhenGetAllFilm_ThenReturnsList()
        {
            // ACT
            var result = this.filmlogic.GetAll();

            // ASSERT
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count());
        }

        /// <summary>
        /// DVDkolcsonzo delete test
        /// </summary>
        public void GivenDVDkolcsonzo_WhenDelete_ReturnsTrue()
        {
            DVDkolcsonzo d = new DVDkolcsonzo()
            {
                Film_ID = 1,
                id = 1,
                Ar = 3000,
                Duplaretegu = false,
                Gyujtoi_kiadas = true,
                Regio_kod = 6,
                Tipus = "fullhd"
            };

            bool result = this.dvdlogic.Delete(d);
            Assert.IsTrue(result);
        }

        /// <summary>
        /// DVDkolcsonzo insert test
        /// </summary>
        [Test]
        public void GivennewDVDkolcsonzo_WhenInsert_ReturnsTrue()
        {
            DVDkolcsonzo d = new DVDkolcsonzo()
            {
                Film_ID = 1,
                id = 1,
                Ar = 3000,
                Duplaretegu = false,
                Gyujtoi_kiadas = true,
                Regio_kod = 6,
                Tipus = "fullhd"
            };

            bool result = this.dvdlogic.Insert(d);
            Assert.IsTrue(result);
        }

        /// <summary>
        /// DVDkolcsonzo update test
        /// </summary>
        [Test]
        public void GivenDVDkolcsonzo_WhenUpdate_ReturnsTrue()
        {
            DVDkolcsonzo d = new DVDkolcsonzo()
            {
                Film_ID = 1,
                id = 1,
                Ar = 3000,
                Duplaretegu = false,
                Gyujtoi_kiadas = true,
                Regio_kod = 6,
                Tipus = "fullhd"
            };

            bool result = this.dvdlogic.Update(d);
            Assert.IsTrue(result);
        }

        /// <summary>
        /// Get dvdkolcsonzo datas test
        /// </summary>
        [Test]
        public void WhenGetAllDVDkolcsonzo_ThenReturnsList()
        {
            var result = this.dvdlogic.GetAll();

            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count());
        }

        /// <summary>
        /// Kolcsonzes delete test
        /// </summary>
        [Test]
        public void GivenKolcsonzes_WhenDelete_ReturnsTrue()
        {
            Kolcsonze k = new Kolcsonze()
            {
                id = 1,
                Film_ID = 1,
                Ugyfel_ID = 1
            };

            bool result = this.kolcsonzeslogic.Delete(k);
            Assert.IsTrue(result);
        }

        /// <summary>
        /// kolcsonzes insert test
        /// </summary>
        [Test]
        public void GivennewKolcsonzes_WhenInsert_ReturnsTrue()
        {
            Kolcsonze k = new Kolcsonze()
            {
                id = 1,
                Film_ID = 1,
                Ugyfel_ID = 1
            };

            bool result = this.kolcsonzeslogic.Insert(k);
            Assert.IsTrue(result);
        }

        /// <summary>
        /// kolcsonzes update test
        /// </summary>
        [Test]
        public void GivenKolcsonzes_WhenUpdate_ReturnsTrue()
        {
            Kolcsonze k = new Kolcsonze()
            {
                id = 1,
                Film_ID = 1,
                Ugyfel_ID = 1
            };

            bool result = this.kolcsonzeslogic.Update(k);
            Assert.IsTrue(result);
        }

        /// <summary>
        /// Get every kolcsonzes test
        /// </summary>
        [Test]
        public void WhenGetAllKolcsonzes_ThenReturnList()
        {
            var result = this.kolcsonzeslogic.GetAll();
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count());
        }

        /// <summary>
        /// NONCrud Nolan producred films test
        /// </summary>
        [Test]
        public void GivenFilms_WhenGetFilmsMadebyChristopherNolan()
        {
            var result = this.filmlogic.GetChristopherNolanFilms();
            Assert.That(result.Count, Is.EqualTo(1));
        }

        /// <summary>
        /// NONCrud under 18 ppl test
        /// </summary>
        [Test]
        public void GivenUgyfels_WhoAreUnderorEven18()
        {
            var result = this.ugyfellogic.GetUgyfelsWhoUnderOrEqual18();
            Assert.That(result.Count, Is.EqualTo(1));
        }

        /// <summary>
        /// NONCrud Bp-n lives test
        /// </summary>
        [Test]
        public void GivenUgyfels_WhenGetUgyfelsWhoAreLivingInBudapest()
        {
            var result = this.ugyfellogic.GetUgyfelWhoLivesInBudapest();
            Assert.That(result.Count, Is.EqualTo(1));
        }
    }
}
