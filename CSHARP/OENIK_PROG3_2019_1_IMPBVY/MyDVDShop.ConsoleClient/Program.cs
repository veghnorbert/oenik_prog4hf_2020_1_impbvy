﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MyDVDShop.ConsoleClient
{

    public class Customer
    {
        public int Customer_ID { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string E_mail { get; set; }
        public int Age { get; set; }
        public string Mobile { get; set; }

        public override string ToString()
        {
            return $"Id={Customer_ID}\tName={Name}\tAddress={Address}\tEmail={E_mail}\tAge={Age}\tMobile={Mobile}";
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Waiting.....");
            Console.ReadLine();

            string url = "http://localhost:51133/api/CustomersApi/";

            using (HttpClient client = new HttpClient())
            {
                string json = client.GetStringAsync(url + "all").Result;
                var list = JsonConvert.DeserializeObject<List<Customer>>(json);
                foreach (var item in list)
                {
                    Console.WriteLine(item);
                }
                Console.ReadLine();

                Dictionary<string, string> postData;
                string response;

                postData = new Dictionary<string, string>();
                postData.Add(nameof(Customer.Name), "Tóth Pista");
                postData.Add(nameof(Customer.Address), "Szekszárd, Arany János utca 4.");
                postData.Add(nameof(Customer.E_mail), "tothpista@gmail.com");
                postData.Add(nameof(Customer.Age), "63");
                postData.Add(nameof(Customer.Mobile), "06307887452");

                response = client.PostAsync(url + "add", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;

                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("Add: " + response);
                Console.WriteLine("All: " + json);
                Console.ReadLine();

                int customerId = JsonConvert.DeserializeObject<List<Customer>>(json).Single(x => x.Name == "Tóth Pista").Customer_ID;
                postData = new Dictionary<string, string>();
                postData.Add(nameof(Customer.Customer_ID), customerId.ToString());
                postData.Add(nameof(Customer.Name), "Tóth Pista");
                postData.Add(nameof(Customer.Address), "Győr, Széchenyi tér 14/B.");
                postData.Add(nameof(Customer.E_mail), "tothpista@gmail.com");
                postData.Add(nameof(Customer.Age), "63");
                postData.Add(nameof(Customer.Mobile), "06307887452");

                response = client.PostAsync(url + "mod", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;

                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("Mod: " + response);
                Console.WriteLine("All: " + json);
                Console.ReadLine();

                response = client.GetStringAsync(url + "del/" + customerId).Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("Del: " + response);
                Console.WriteLine("All: " + json);
                Console.ReadLine();

            }
        }
    }
}
