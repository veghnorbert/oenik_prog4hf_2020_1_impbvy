﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyDVDShop.Web.Models
{
    public class Customer
    {
        [Display(Name = "Customer ID")]
        [Required]
        public int Customer_ID { get; set; }
        [Display(Name = "Customer Name")]
        [Required]
        public string Name { get; set; }
        [Display(Name = "Customer Address")]
        [Required]
        public string Address { get; set; }
        [Display(Name = "Customer E-mail")]
        [Required]
        public string E_mail { get; set; }
        [Display(Name = "Customer Age")]
        [Required]
        public int Age { get; set; }
        [Display(Name = "Customer Mobile")]
        [Required]
        public string Mobile { get; set; }
    }
}