package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "queried")
@XmlAccessorType(XmlAccessType.FIELD)
public class QueriedDVD implements Serializable{
    
   @XmlElement(name = "kolcsonzo")
   private DVDkolcsonzo kolcsonzo;
   
   @XmlElement(name = "filmek")
   private Film filmek;
   
   @XmlElement(name = "film")
   private List<Film> films;

    public Film getFilmek(){
        return filmek;
    }
      
    public DVDkolcsonzo getDVDkolcsonzo() {
        return kolcsonzo;
    }

    public List<Film> getFilms() {
        return films;
    }

    public void setFilm(DVDkolcsonzo kolcsonzo) {
        this.kolcsonzo = kolcsonzo ;
    }
   
    public void setQueriedList(ArrayList<Film> list) {
        this.films = list;
        
    }
    
}
