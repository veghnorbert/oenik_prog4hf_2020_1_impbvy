var interface_my_d_v_d_shop_1_1_logic_1_1_i_logic =
[
    [ "Delete", "interface_my_d_v_d_shop_1_1_logic_1_1_i_logic.html#aa026fe007d05bf88d3be27a806a27a52", null ],
    [ "GetAll", "interface_my_d_v_d_shop_1_1_logic_1_1_i_logic.html#a23876b87a6a2b9264f470a17596f3ac4", null ],
    [ "GetChristopherNolanFilms", "interface_my_d_v_d_shop_1_1_logic_1_1_i_logic.html#a41f33695446f6b7bdbb349286daae78c", null ],
    [ "GetUgyfelsWhoUnderOrEqual18", "interface_my_d_v_d_shop_1_1_logic_1_1_i_logic.html#ae652223f994d80e2770b539c31da9804", null ],
    [ "GetUgyfelWhoLivesInBudapest", "interface_my_d_v_d_shop_1_1_logic_1_1_i_logic.html#a0b47fa411778fe0c3c1cd1b00205dab0", null ],
    [ "Insert", "interface_my_d_v_d_shop_1_1_logic_1_1_i_logic.html#a08cda2afe4ba43b560450e80f3f2b2ac", null ],
    [ "JavaEndPoint", "interface_my_d_v_d_shop_1_1_logic_1_1_i_logic.html#ae7e3292fcae8a84dfd34cc5f66b64777", null ],
    [ "Update", "interface_my_d_v_d_shop_1_1_logic_1_1_i_logic.html#a95e4691d37aa4bf22fce108a923747ef", null ]
];