﻿using AutoMapper;
using MyDVDShop.Data;
using MyDVDShop.Logic;
using MyDVDShop.Repository;
using MyDVDShop.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyDVDShop.Web.Controllers
{
    public class CustomersController : Controller
    {
        IMapper mapper;
        CustomersViewModel vm;
        IUgyfelLogic logic;

        //public CustomersController(ILogic<Ugyfel> logic, IMapper mapper)

        public CustomersController()
        {
            DVDShopDBContext db = new DVDShopDBContext();
            UgyfelRepository ugyfelRepository = new UgyfelRepository(db);
            logic = new UgyfelLogic(ugyfelRepository);
            mapper = MapperFactory.CreateMapper();

            vm = new CustomersViewModel();
            vm.EditedCustomer = new Customer();
            var ugyfels = logic.GetAllUgyfel();
            vm.ListOfCustomers = mapper.Map<IList<Data.Ugyfel>,List<Models.Customer>>(ugyfels);
        }

        private Customer GetCustomerModel(int id)
        {
            Ugyfel oneCustomer = logic.GetOneUgyfel(id);
            return mapper.Map<Data.Ugyfel, Models.Customer>(oneCustomer);
        }

        // GET: Customers
        public ActionResult Index()
        {
            ViewData["editAction"] = "AddNew";
            return View("CustomerIndex",vm);
        }

        // GET: Customers/Details/5
        public ActionResult Details(int id)
        {
            return View("CustomerDetails", GetCustomerModel(id));
        }

        // GET: /Customers/Delete/5
        public ActionResult Delete(int id)
        {
            TempData["editResult"] = "Delete Failed";
            if (logic.Remove(id)) TempData["editResult"] = "Delete OK";
            return RedirectToAction(nameof(Index));
        }

        // GET: /Customers/Edit/5
        public ActionResult Edit(int id)
        {
            ViewData["editAction"] = "Edit";
            vm.EditedCustomer = GetCustomerModel(id);
            return View("CustomerIndex", vm);
        }

        //POST: /Customer/Edit/ + Customer + editAction
        [HttpPost]
        public ActionResult Edit(Customer customer,string editAction)
        {
            if (ModelState.IsValid && customer != null)
            {
                TempData["editResult"] = "Edit OK";
                if (editAction == "AddNew")
                {
                    logic.AddUgyfel(customer.Name,customer.Address,customer.E_mail,customer.Age,customer.Mobile);
                }
                else
                {
                    bool success = logic.ChangeUgyfel(customer.Customer_ID,customer.Name, customer.Address, customer.E_mail, customer.Age, customer.Mobile);
                    if (!success) TempData["editResult"] = "Edit Failed";
                }
                return RedirectToAction(nameof(Index),vm);
            }
            else
            {
                ViewData["editAction"] = "Edit";
                vm.EditedCustomer = customer;
                return View("CustomerIndex", vm);
            }
        }




    }
}