﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyDVDShop.Repository
{
    using System.Collections.Generic;
    using MyDVDShop.Data;

    /// <summary>
    /// This is the interface of the repository.cs
    /// </summary>
    /// <typeparam name="T"> T can be Ugyfel, Film, Kolcsonzes, DVDkolcsonzo</typeparam>
    public interface IRepository<T>
        where T : class
    {
        /// <summary>
        /// It transfers the data from the program.cs to the Insert method in the Repository.cs, and gives back the returned parameter from the Repository.cs
        /// </summary>
        /// <param name="obj">This method brings the object type to the Repository.cs</param>
        /// <returns> It returns with a boolean, to check if the Insert Method was successful </returns>
        bool Insert(T obj);

        /// <summary>
        /// This is the GetAll method which return an entity from the database
        /// </summary>
        /// <returns>It returns an entity from the database which can be Film,Ugyfel,Kolcsonzes,DVDkolcsonzo</returns>
        IEnumerable<T> GetAll();

        /// <summary>
        /// It transfers the data from the program.cs to the Update method in the Repository.cs, and gives back the returned parameter from the Repository.cs
        /// </summary>
        /// <param name="obj">This method brings the object type to the Repository.cs</param>
        /// <returns> It returns with a boolean, to check if the Update Method was successful </returns>
        bool Update(T obj);

        /// <summary>
        /// It transfers the data from the program.cs to the Delete method in the Repository.cs, and gives back the returned parameter from the Repository.cs
        /// </summary>
        /// <param name="obj">This method brings the object type to the Repository.cs</param>
        /// <returns> It returns with a boolean, to check if the Delte Method was successful </returns>
        bool Delete(T obj);

        T GetOne(int id);

        bool Remove(int id);
    }

    public interface IUgyfelRepository : IRepository<Ugyfel>
    {
        bool ChangeUgyfel(int id, string name, string address, string email, int age, string mobile);
    }
}