var searchData=
[
  ['givendvdkolcsonzo_5fwhendelete_5freturnstrue',['GivenDVDkolcsonzo_WhenDelete_ReturnsTrue',['../class_my_d_v_d_shop_1_1_logic_1_1_tests_1_1_test.html#a258ddd1760aa811c8411915de677a4ea',1,'MyDVDShop::Logic::Tests::Test']]],
  ['givendvdkolcsonzo_5fwhenupdate_5freturnstrue',['GivenDVDkolcsonzo_WhenUpdate_ReturnsTrue',['../class_my_d_v_d_shop_1_1_logic_1_1_tests_1_1_test.html#afdd19072e298b20d1de0ed879b3306c3',1,'MyDVDShop::Logic::Tests::Test']]],
  ['givenfilm_5fwhendelete_5freturnstrue',['GivenFilm_WhenDelete_ReturnsTrue',['../class_my_d_v_d_shop_1_1_logic_1_1_tests_1_1_test.html#a69c79ef5503eaa4db8b4adc2a0f9970d',1,'MyDVDShop::Logic::Tests::Test']]],
  ['givenfilm_5fwhenupdate_5freturnstrue',['GivenFilm_WhenUpdate_ReturnsTrue',['../class_my_d_v_d_shop_1_1_logic_1_1_tests_1_1_test.html#adf59639c286357499c191d2037dacc6e',1,'MyDVDShop::Logic::Tests::Test']]],
  ['givenfilms_5fwhengetfilmsmadebychristophernolan',['GivenFilms_WhenGetFilmsMadebyChristopherNolan',['../class_my_d_v_d_shop_1_1_logic_1_1_tests_1_1_test.html#ac9bf84f2dbd0e57850d23118f0e28446',1,'MyDVDShop::Logic::Tests::Test']]],
  ['givenkolcsonzes_5fwhendelete_5freturnstrue',['GivenKolcsonzes_WhenDelete_ReturnsTrue',['../class_my_d_v_d_shop_1_1_logic_1_1_tests_1_1_test.html#a2ea08e8a65dc5f6d8fabdb0ebdebb979',1,'MyDVDShop::Logic::Tests::Test']]],
  ['givenkolcsonzes_5fwhenupdate_5freturnstrue',['GivenKolcsonzes_WhenUpdate_ReturnsTrue',['../class_my_d_v_d_shop_1_1_logic_1_1_tests_1_1_test.html#adebbd6fd6189328addaa8f66387905ab',1,'MyDVDShop::Logic::Tests::Test']]],
  ['givennewdvdkolcsonzo_5fwheninsert_5freturnstrue',['GivennewDVDkolcsonzo_WhenInsert_ReturnsTrue',['../class_my_d_v_d_shop_1_1_logic_1_1_tests_1_1_test.html#a090658a53c667125582b5be77b2ff832',1,'MyDVDShop::Logic::Tests::Test']]],
  ['givennewfilm_5fwheninsert_5freturnstrue',['GivennewFilm_WhenInsert_ReturnsTrue',['../class_my_d_v_d_shop_1_1_logic_1_1_tests_1_1_test.html#a80486ae20703a24ea7f9ec610f67372b',1,'MyDVDShop::Logic::Tests::Test']]],
  ['givennewkolcsonzes_5fwheninsert_5freturnstrue',['GivennewKolcsonzes_WhenInsert_ReturnsTrue',['../class_my_d_v_d_shop_1_1_logic_1_1_tests_1_1_test.html#ab97c7c107e7d3a80507a9226076428e8',1,'MyDVDShop::Logic::Tests::Test']]],
  ['givennewugyfel_5fwheninsert_5freturnstrue',['GivennewUgyfel_WhenInsert_ReturnsTrue',['../class_my_d_v_d_shop_1_1_logic_1_1_tests_1_1_test.html#acd26e6fb10e6ef3aa8d95ac651b96914',1,'MyDVDShop::Logic::Tests::Test']]],
  ['givenugyfel_5fwhendelete_5freturnstrue',['GivenUgyfel_WhenDelete_ReturnsTrue',['../class_my_d_v_d_shop_1_1_logic_1_1_tests_1_1_test.html#a1bdb5013faa7291f16fe49ec5b141637',1,'MyDVDShop::Logic::Tests::Test']]],
  ['givenugyfel_5fwhenupdate_5freturnstrue',['GivenUgyfel_WhenUpdate_ReturnsTrue',['../class_my_d_v_d_shop_1_1_logic_1_1_tests_1_1_test.html#ae4438c4219c2c04e856d90961688f905',1,'MyDVDShop::Logic::Tests::Test']]],
  ['givenugyfels_5fwhengetugyfelswhoarelivinginbudapest',['GivenUgyfels_WhenGetUgyfelsWhoAreLivingInBudapest',['../class_my_d_v_d_shop_1_1_logic_1_1_tests_1_1_test.html#a28994946d62e089519ccde7915535b39',1,'MyDVDShop::Logic::Tests::Test']]],
  ['givenugyfels_5fwhoareunderoreven18',['GivenUgyfels_WhoAreUnderorEven18',['../class_my_d_v_d_shop_1_1_logic_1_1_tests_1_1_test.html#aa988944891abe1b73dfed4c2df7d70da',1,'MyDVDShop::Logic::Tests::Test']]]
];
