﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyDVDShop.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MyDVDShop.Data;
    using MyDVDShop.Logic;
    using MyDVDShop.Repository;

    /// <summary>
    /// This is the UI
    /// </summary>
    public class Program
    {
        private static int index = 0;
        private static DVDShopDBContext db = new DVDShopDBContext();
        private static ILogic<Film> filmLogic = new Logic<Film>(new Repository<Film>(db));
        private static ILogic<Ugyfel> ugyfelLogic = new Logic<Ugyfel>(new Repository<Ugyfel>(db));
        private static ILogic<Kolcsonze> kolcsonzesLogic = new Logic<Kolcsonze>(new Repository<Kolcsonze>(db));

        /// <summary>
        /// The UI main method
        /// </summary>
        /// <param name="args">beginning parameter for the main method</param>
        public static void Main(string[] args)
        {
            while (index < 17)
            {
                Process();
            }

            Environment.Exit(0);
        }

        /// <summary>
        /// Menu
        /// </summary>
        public static void Menü()
        {
            Console.Clear();
            Console.WriteLine("1.) Film listázás");
            Console.WriteLine("2.) Kölcsönzés listázás");
            Console.WriteLine("3.) Ügyfél listázás");
            Console.WriteLine("---------------------");
            Console.WriteLine("4.) Film hozzáadása");
            Console.WriteLine("5.) Kölcsönzés hozzáadása ");
            Console.WriteLine("6.) Ügyfél hozzáadása");
            Console.WriteLine("---------------------");
            Console.WriteLine("7.) Film törlése");
            Console.WriteLine("8.) Kölcsönzés törlése");
            Console.WriteLine("9.) Ügyfél törlése");
            Console.WriteLine("---------------------");
            Console.WriteLine("10.) Update Film");
            Console.WriteLine("11.) Update Kölcsönzés");
            Console.WriteLine("12.) Update Ügyfél");
            Console.WriteLine("---------------------");
            Console.WriteLine("13.) NonCRUD1");
            Console.WriteLine("---------------------");
            Console.WriteLine("14.) NonCRUD2");
            Console.WriteLine("---------------------");
            Console.WriteLine("15.) NonCRUD3");
            Console.WriteLine("---------------------");
            Console.WriteLine("16.) Java Endpoint");
            Console.WriteLine("---------------------");
            Console.WriteLine("17.) Exit");
            Console.WriteLine("---------------------");
        }

        /// <summary>
        /// Methods
        /// </summary>
        /// <param name="index">index</param>
        public static void DBMunka(int index)
        {
            if (index == 1)
            {
                var eredmeny = filmLogic.GetAll();
                foreach (var item in eredmeny)
                {
                    Console.WriteLine(item.ToString());
                }

                Console.ReadLine();
            }
            else if (index == 2)
            {
                var eredmeny = kolcsonzesLogic.GetAll();
                foreach (var item in eredmeny)
                {
                    Console.WriteLine(item.ToString());
                }

                Console.ReadLine();
            }
            else if (index == 3)
            {
                var eredmeny = ugyfelLogic.GetAll();
                foreach (var item in eredmeny)
                {
                    Console.WriteLine(item.ToString());
                }

                Console.ReadLine();
            }
            else if (index == 4)
            {
                Film ujFilm = new Film
                {
                    DVDkolcsonzo_id = 1,
                    Cim = "Végjáték",
                    Rendezo = "Anthony Russo, Joe Russo",
                    Film_Studio = "Marvel",
                    Megjelenes_ideje = "2019.04.25",
                    Bevetel = 356000000
                };

                filmLogic.Insert(ujFilm);
                Console.WriteLine("Film hozzáadva! " + ujFilm.Cim);
                Console.ReadLine();
            }
            else if (index == 5)
            {
                Kolcsonze ujKolcsonzes = new Kolcsonze
                {
                    Film_ID = 1,
                    Ugyfel_ID = 1
                };

                kolcsonzesLogic.Insert(ujKolcsonzes);
                Console.WriteLine("Kölcsönzés hozzáadva! ");
                Console.ReadLine();
            }
            else if (index == 6)
            {
                Ugyfel ujUgyfel = new Ugyfel
                {
                    Nev = "Pápai Joci",
                    Cim = "budapest",
                    E_mail = "papai@joci.hu",
                    Eletkor = 16,
                    Telefon = "+36709496012"
                };

                ugyfelLogic.Insert(ujUgyfel);
                Console.WriteLine("Ügyfél hozzáadva! " + ujUgyfel.Nev);
                Console.ReadLine();
            }
            else if (index == 7)
            {
                Console.WriteLine("Melyik Filmet szeretnéd törölni?");

                string whichtodel = Console.ReadLine().ToLower();

                var q = filmLogic.GetAll().Where(x => x.Cim.ToLower() == whichtodel).FirstOrDefault();

                filmLogic.Delete(q);

                Console.WriteLine("Film törölve!");
                Console.ReadLine();
            }
            else if (index == 8)
            {
                Console.WriteLine("Melyik kölcsönzést szeretnéd törölni?  (id alapján)");

                int which = int.Parse(Console.ReadLine());

                var q = kolcsonzesLogic.GetAll().Where(x => x.id == which).FirstOrDefault();

                kolcsonzesLogic.Delete(q);

                Console.WriteLine("Kölcsönzés törölve!");
                Console.ReadLine();
            }
            else if (index == 9)
            {
                Console.WriteLine("Melyik ügyfelet szeretnéd törölni?");

                string which = Console.ReadLine().ToLower();

                var q = ugyfelLogic.GetAll().Where(x => x.Nev.ToLower() == which).FirstOrDefault();

                ugyfelLogic.Delete(q);

                Console.WriteLine("Ügyfél törölve!");
                Console.ReadLine();
            }
            else if (index == 10)
            {
                Console.WriteLine("Melyik filmet szeretnéd updatelni?");

                int whichone = int.Parse(Console.ReadLine());

                Film f = new Film();
                var fUpdate = filmLogic.GetAll();

                foreach (var item in fUpdate)
                {
                    if (item.Film_ID == whichone)
                    {
                        f = item;
                        break;
                    }
                }

                f.Cim = "The Expendables";
                var result = filmLogic.Update(f);
                if (result)
                {
                    Console.WriteLine("Film updatelve!" + f.ToString());
                }
                else
                {
                    throw new Exception("Hiba!");
                }

                Console.ReadLine();
            }
            else if (index == 11)
            {
                Console.WriteLine("Melyik kölcsönzést szeretnéd updatelni? (id alapján)");

                int whichone = int.Parse(Console.ReadLine());
                Kolcsonze k = new Kolcsonze();
                var kUpdate = kolcsonzesLogic.GetAll();

                foreach (var item in kUpdate)
                {
                    if (item.id == whichone)
                    {
                        k = item;
                        break;
                    }
                }

                k.Ugyfel_ID = 1;
                var result = kolcsonzesLogic.Update(k);
                if (result)
                {
                    Console.WriteLine("Kölcsönzés updatelve!" + k.ToString());
                }
                else
                {
                    throw new Exception("Hiba!");
                }
            }
            else if (index == 12)
            {
                Console.WriteLine("Melyik ügyfelet szeretnéd updatelni? (id alapján)");

                int whichone = int.Parse(Console.ReadLine());
                Ugyfel u = new Ugyfel();
                var uUpdate = ugyfelLogic.GetAll();

                foreach (var item in uUpdate)
                {
                    if (item.Ugyfel_ID == whichone)
                    {
                        u = item;
                        break;
                    }
                }

                u.Nev = "Hegedűs Péter";
                var result = ugyfelLogic.Update(u);
                if (result)
                {
                    Console.WriteLine("Ügyfél updatelve!" + u.ToString());
                }
                else
                {
                    throw new Exception("Hiba!");
                }

                Console.ReadLine();
            }
            else if (index == 13)
            {
                var result = filmLogic.GetChristopherNolanFilms();
                foreach (var item in result)
                {
                    Console.WriteLine(item);
                }

                Console.ReadLine();
            }
            else if (index == 14)
            {
                var result = ugyfelLogic.GetUgyfelsWhoUnderOrEqual18();
                foreach (var item in result)
                {
                    Console.WriteLine(item);
                }

                Console.ReadLine();
            }
            else if (index == 15)
            {
                var result = ugyfelLogic.GetUgyfelWhoLivesInBudapest();
                foreach (var item in result)
                {
                    Console.WriteLine(item);
                }

                Console.ReadLine();
            }
            else if (index == 16)
            {
                 var q = kolcsonzesLogic.JavaEndPoint();

                foreach (var item in q)
                {
                    Console.Write(item);
                }

                Console.ReadLine();
            }
            else if (index == 17)
            {
                return;
            }
            else
            {
                Console.WriteLine("Rossz menü elemet válaszott! Kérem válasszon a felsorolt menüelemek közül!");
            }

            Console.WriteLine("A folytatáshoz nyomjon le egy billentyűt!");
            Console.ReadLine();
        }

        /// <summary>
        /// Process calls Menu, then waiting for the index from the console
        /// </summary>
        public static void Process()
        {
            Menü();

            try
            {
                index = int.Parse(Console.ReadLine());
                DBMunka(index);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}