var class_my_d_v_d_shop_1_1_logic_1_1_logic =
[
    [ "Logic", "class_my_d_v_d_shop_1_1_logic_1_1_logic.html#a8a89334c4cc633369cf171b9b2e47be4", null ],
    [ "Delete", "class_my_d_v_d_shop_1_1_logic_1_1_logic.html#a52fbfc19bcf7f2ae32b49f0fcb5a65df", null ],
    [ "GetAll", "class_my_d_v_d_shop_1_1_logic_1_1_logic.html#a4232c5980d653d4342eadc3e098c2083", null ],
    [ "GetChristopherNolanFilms", "class_my_d_v_d_shop_1_1_logic_1_1_logic.html#aad1f1091bb7d1b50b6c5c7eb24345ff3", null ],
    [ "GetUgyfelsWhoUnderOrEqual18", "class_my_d_v_d_shop_1_1_logic_1_1_logic.html#a99fc68d953324dd07679dbfb34bae1c8", null ],
    [ "GetUgyfelWhoLivesInBudapest", "class_my_d_v_d_shop_1_1_logic_1_1_logic.html#ab2afc44d31cf24f82638cea7f6f70d8b", null ],
    [ "Insert", "class_my_d_v_d_shop_1_1_logic_1_1_logic.html#a6fb224f16e1fabd77c9d821d2ebcb77c", null ],
    [ "JavaEndPoint", "class_my_d_v_d_shop_1_1_logic_1_1_logic.html#a26d6a7330dcbeda55fc7afff9cbe34a7", null ],
    [ "Update", "class_my_d_v_d_shop_1_1_logic_1_1_logic.html#a3bd55f67c25c2f09893e0d0829b06597", null ]
];