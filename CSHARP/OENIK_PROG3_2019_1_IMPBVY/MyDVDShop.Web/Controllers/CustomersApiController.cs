﻿using AutoMapper;
using MyDVDShop.Data;
using MyDVDShop.Logic;
using MyDVDShop.Repository;
using MyDVDShop.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MyDVDShop.Web.Controllers
{
    public class CustomersApiController : ApiController
    {
        public class ApiResult
        {
            public bool OperationResult { get; set; }
        }

        IUgyfelLogic logic;
        IMapper mapper;
        public CustomersApiController()
        {
            DVDShopDBContext db = new DVDShopDBContext();
            UgyfelRepository ugyfelRepository = new UgyfelRepository(db);
            logic = new UgyfelLogic(ugyfelRepository);
            mapper = MapperFactory.CreateMapper();
        }


        [ActionName("all")]
        [HttpGet]
        // GET api/CustomersApi/all
        public IEnumerable<Models.Customer> GetAll()
        {
            var customers = logic.GetAllUgyfel();
            return mapper.Map<IList<Data.Ugyfel>, List<Models.Customer>>(customers);
        }

        // GET api/CustomersApi/del/42
        [ActionName("del")]
        [HttpGet]
        public ApiResult DelOneCustomer(int id)
        {
            bool success = logic.Remove(id);
            return new ApiResult() { OperationResult = true };
        }

        // GET api/CustomersApi/add + customer
        [ActionName("add")]
        [HttpPost]
        public ApiResult AddOneCustomer(Customer customer)
        {
            logic.AddUgyfel(customer.Name, customer.Address, customer.E_mail, customer.Age, customer.Mobile);
            return new ApiResult() { OperationResult = true };
        }

        // GET api/CustomersApi/mod + customer
        [ActionName("mod")]
        [HttpPost]
        public ApiResult ModOneCustomer(Customer customer)
        {
            bool success = logic.ChangeUgyfel(customer.Customer_ID,customer.Name, customer.Address, customer.E_mail, customer.Age, customer.Mobile);
            return new ApiResult() { OperationResult = true };

        }
    }
}
