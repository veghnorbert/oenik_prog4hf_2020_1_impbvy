package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name ="kolcsonzo")
@XmlAccessorType(XmlAccessType.FIELD)
public class DVDkolcsonzo implements Serializable{
    
   private static int nextid;
   private static int nextfilmid;
   @XmlElement
   private int id;
   @XmlElement
   private int filmid;
   @XmlElement
    private String cim;
   @XmlElement
   private String tipus;
   @XmlElement
   private int ar;
   @XmlElement
   private boolean duplaretegu;
   @XmlElement
   private boolean gyujtoikiadas;
   @XmlElement
   private int regiokod;
   @XmlElement
   private List<Film> films = new ArrayList<>();
   
   
    public DVDkolcsonzo() {
        this.id = nextid++;
        this.filmid = nextfilmid++;
    }

    public List<Film> getFilms() {
        return films;
    }

    
    public String getCim() {
        return cim;
    }

    public void setCim(String cim) {
        this.cim = cim;
    }
    
    public int getId()
    {
        return this.id;
    }
    public int getfilmid()
    {
        return this.filmid;
    }
    
    public String getTipus() {
        return tipus;
    }

    public void setTipus(String tipus) {
        this.tipus = tipus;
    }

    public int getAr() {
        return ar;
    }

    public void setAr(int ar) {
        this.ar = ar;
    }

    public boolean isDuplaretegu() {
        return duplaretegu;
    }

    public void setDuplaretegu(boolean duplaretegu) {
        this.duplaretegu = duplaretegu;
    }

    public boolean isGyujtoikiadas() {
        return gyujtoikiadas;
    }

    public void setGyujtoikiadas(boolean gyujtoikiadas) {
        this.gyujtoikiadas = gyujtoikiadas;
    }

    public int getRegiokod() {
        return regiokod;
    }

    public void setRegiokod(int regiokod) {
        this.regiokod = regiokod;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DVDkolcsonzo other = (DVDkolcsonzo) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }
   
   
   
   
   
}
