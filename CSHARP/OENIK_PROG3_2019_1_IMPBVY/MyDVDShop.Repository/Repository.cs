﻿// <copyright file="Repository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyDVDShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using MyDVDShop.Data;

    /// <summary>
    /// This is the Repository class
    /// </summary>
    /// <typeparam name="T">T can be Film, DVDkolcsonzo, Ugyfel, Kolcsonzes</typeparam>
    public class Repository<T> : IRepository<T>
        where T : class
    {
        protected DVDShopDBContext db;

        public Repository(DVDShopDBContext db)
        {
            this.db = db;
        }

        /// <summary>
        /// It transfers the data from the program.cs to the Insert method in the Repository.cs, and gives back the returned parameter from the Repository.cs
        /// </summary>
        /// <param name="obj">This method brings the object type to the Repository.cs</param>
        /// <returns> It returns with a boolean, to check if the Insert Method was successful </returns>
        public bool Insert(T obj)
        {
            try
            {
                this.db.Set<T>().Add(obj);
                this.db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// It transfers the data from the program.cs to the Delete method in the Repository.cs, and gives back the returned parameter from the Repository.cs
        /// </summary>
        /// <param name="obj">This method brings the object type to the Repository.cs</param>
        /// <returns> It returns with a boolean, to check if the Delte Method was successful </returns>
        public bool Delete(T obj)
        {
            try
            {
                this.db.Set<T>().Remove(obj);
                this.db.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// This is the GetAll method which return an entity from the database
        /// </summary>
        /// <returns>It returns an entity from the database which can be Film,Ugyfel,Kolcsonzes,DVDkolcsonzo</returns>
        public IEnumerable<T> GetAll()
        {
            return this.db.Set<T>().AsEnumerable();
        }

        /// <summary>
        /// It transfers the data from the program.cs to the Update method in the Repository.cs, and gives back the returned parameter from the Repository.cs
        /// </summary>
        /// <param name="obj">This method brings the object type to the Repository.cs</param>
        /// <returns> It returns with a boolean, to check if the Update Method was successful </returns>
        public bool Update(T obj)
        {
            try
            {
                this.db.Entry(obj).State = EntityState.Modified;
                this.db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public T GetOne(int id)
        {
            return this.db.Set<T>().Find(id);
        }

        public bool Remove(int id)
        {
            T entity = this.GetOne(id);
            if (entity == null)
            {
                return false;
            }
            else
            {
                this.db.Set<T>().Remove(entity);
                this.db.SaveChanges();
                return true;
            }
        }
    }

    public class UgyfelRepository : Repository<Ugyfel>, IUgyfelRepository
    {
        public UgyfelRepository(DVDShopDBContext db)
            : base(db)
        {
        }

        public bool ChangeUgyfel(int id, string name, string address, string email, int age, string mobile)
        {
            Ugyfel entity = this.GetOne(id);
            if (entity == null)
            {
                return false;
            }
            else
            {
                entity.Nev = name;
                entity.Cim = address;
                entity.E_mail = email;
                entity.Eletkor = age;
                entity.Telefon = mobile;
                this.db.SaveChanges();
                return true;
            }
        }
    }
}
