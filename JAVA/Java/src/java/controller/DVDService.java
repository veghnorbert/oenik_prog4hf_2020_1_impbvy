package controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import model.DVDkolcsonzo;
import model.Film;
import model.QueriedDVD;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class DVDService implements Serializable{
    
    private static DVDService instance;

    public DVDService() {
        DVDkolcsonzo kolcs = new DVDkolcsonzo();
        kolcs.setTipus("fullhd");
        kolcs.setAr(5000);
        kolcs.setCim("remenyrabjai");
        kolcs.setDuplaretegu(false);
        kolcs.setRegiokod(6);
        kolcs.setGyujtoikiadas(false);
        
    
        Film f = new Film();
        f.setCim("remenyrabjai");
        f.setRendezo("Frank Darabont");
        f.setFilmstudio("Castle Rock Entertainment");
        f.setMegjelendesideje("1994.09.23");
        f.setBevetel(59800000);
        
        kolcs.getFilms().add(f);
        
    kolcsonzo.add(kolcs);
    
    }
    public static DVDService getInstance(){
        if (instance == null) {
            instance = new DVDService();
        }
        return instance;
    }
    
    
    @XmlElement
    private List<DVDkolcsonzo> kolcsonzo = new ArrayList<>();
       
    public List<DVDkolcsonzo> getKolcsonzo() {
        return kolcsonzo;
    }
    
    public QueriedDVD getQueriedDVD(String kolcsonzok)
    {
        QueriedDVD local = new QueriedDVD();
        for (DVDkolcsonzo d : kolcsonzo) {
            if (d.getCim().toLowerCase().trim().equals(kolcsonzok.toLowerCase().trim())) {
                local.setQueriedList((ArrayList<Film>) d.getFilms());
            }
        }        
        return local;
    }
    
}
