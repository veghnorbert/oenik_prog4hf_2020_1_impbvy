﻿// <copyright file="ILogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyDVDShop.Logic
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MyDVDShop.Data;

    /// <summary>
    /// This is the interface of the Logic class
    /// </summary>
    /// <typeparam name="T">This can be Film, DVDkolcsonzo, Ugyfel, Kolcsonzes</typeparam>
    public interface ILogic<T>
        where T : class
    {
        /// <summary>
        /// This is the GetAll method which return an entity from the database
        /// </summary>
        /// <returns>It returns an entity from the database which can be Film,Ugyfel,Kolcsonzes,DVDkolcsonzo</returns>
        IEnumerable<T> GetAll();

        /// <summary>
        /// It transfers the data from the program.cs to the Insert method in the Repository.cs, and gives back the returned parameter from the Repository.cs
        /// </summary>
        /// <param name="obj">This method brings the object type to the Repository.cs</param>
        /// <returns> It returns with a boolean, to check if the Insert Method was successful </returns>
        bool Insert(T obj);

        /// <summary>
        /// It transfers the data from the program.cs to the Update method in the Repository.cs, and gives back the returned parameter from the Repository.cs
        /// </summary>
        /// <param name="obj">This method brings the object type to the Repository.cs</param>
        /// <returns> It returns with a boolean, to check if the Update Method was successful </returns>
        bool Update(T obj);

        /// <summary>
        /// It transfers the data from the program.cs to the Delete method in the Repository.cs, and gives back the returned parameter from the Repository.cs
        /// </summary>
        /// <param name="obj">This method brings the object type to the Repository.cs</param>
        /// <returns> It returns with a boolean, to check if the Delte Method was successful </returns>
        bool Delete(T obj);

        /// <summary>
        /// Noncrud
        /// </summary>
        /// <returns>Returns the under 18 or even ugyfels</returns>
        IEnumerable<Ugyfel> GetUgyfelsWhoUnderOrEqual18();

        /// <summary>
        /// Noncrud
        /// </summary>
        /// <returns>return the ugyfels who are living in budapest</returns>
        IEnumerable<Ugyfel> GetUgyfelWhoLivesInBudapest();

        /// <summary>
        /// Noncrud
        /// </summary>
        /// <returns> returns the nolan producered films</returns>
        IEnumerable<Film> GetChristopherNolanFilms();

        /// <summary>
        /// JAVA
        /// </summary>
        /// <returns> JAVA XML</returns>
        string JavaEndPoint();

        T GetOne(int id);
    }

    public interface IUgyfelLogic
    {
        bool ChangeUgyfel(int id, string name, string address, string email, int age, string mobile);

        bool Remove(int id);

        void AddUgyfel(string name, string address, string email, int age, string mobile);

        Ugyfel GetOneUgyfel(int id);

        IList<Ugyfel> GetAllUgyfel();
    }
}
