﻿using GalaSoft.MvvmLight.Messaging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MyDVDShop.WPF
{
    class MainLogic
    {
        string url = "http://localhost:51133/api/CustomersApi/";
        HttpClient client = new HttpClient();

        void SendMessage(bool success)
        {
            string msg = success ? "Operation completed" : "Operation failed";
            Messenger.Default.Send(msg, "CustomerResult");
        }

        public List<CustomerVM> ApiGetCustomers()
        {
            string json = client.GetStringAsync(url +"all").Result;
            var list = JsonConvert.DeserializeObject<List<CustomerVM>>(json);
            //SendMessage(true);
            return list;
        }

        public void ApiDelCar(CustomerVM customer)
        {
            bool success = false;
            if (customer != null)
            {
                string json = client.GetStringAsync(url + "del/" + customer.Customer_ID).Result;
                JObject obj = JObject.Parse(json);
                success = (bool)obj["OperationResult"];
            }
            SendMessage(success);
        }

        bool ApiEditCustomer(CustomerVM customer, bool isEditing)
        {
            if (customer == null) return false;
            string myurl = isEditing ? url + "mod" : url + "add";

            Dictionary<string, string> postData = new Dictionary<string, string>();
            if (isEditing) postData.Add(nameof(CustomerVM.Customer_ID), customer.Customer_ID.ToString());
            postData.Add(nameof(CustomerVM.Name), customer.Name);
            postData.Add(nameof(CustomerVM.Address), customer.Address);
            postData.Add(nameof(CustomerVM.E_mail), customer.E_mail);
            postData.Add(nameof(CustomerVM.Age), customer.Age.ToString());
            postData.Add(nameof(CustomerVM.Mobile), customer.Mobile);

            string json = client.PostAsync(myurl, new FormUrlEncodedContent
                (postData)).Result.Content.ReadAsStringAsync().Result;
            JObject obj = JObject.Parse(json);
            return (bool)obj["OperationResult"];
        }
        public void EditCustomer(CustomerVM customer, Func<CustomerVM, bool> editor)
        {
            CustomerVM clone = new CustomerVM();
            if (customer != null) clone.CopyFrom(customer);
            bool? success = editor?.Invoke(clone);
            if (success == true)
            {
                if (customer != null) success = ApiEditCustomer(clone, true);
                else success = ApiEditCustomer(clone, false);
            }
            SendMessage(success == true);
        }
    }
}
