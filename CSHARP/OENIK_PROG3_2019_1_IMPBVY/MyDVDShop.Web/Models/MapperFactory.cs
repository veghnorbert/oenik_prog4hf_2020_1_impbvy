﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyDVDShop.Web.Models
{
    public class MapperFactory
    {
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<MyDVDShop.Data.Ugyfel, MyDVDShop.Web.Models.Customer>().
                ForMember(dest => dest.Customer_ID, map => map.MapFrom(src => src.Ugyfel_ID)).
                ForMember(dest => dest.Name, map => map.MapFrom(src => src.Nev)).
                ForMember(dest => dest.Address, map => map.MapFrom(src => src.Cim)).
                ForMember(dest => dest.E_mail, map => map.MapFrom(src => src.E_mail)).
                ForMember(dest => dest.Age, map => map.MapFrom(src => src.Eletkor)).
                ForMember(dest => dest.Mobile, map => map.MapFrom(src => src.Telefon));
            });
            return config.CreateMapper();
        }
        
    }
}