﻿// <copyright file="Logic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyDVDShop.Logic
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml.Linq;
    using MyDVDShop.Data;
    using MyDVDShop.Repository;

    /// <summary>
    /// Logic osztály
    /// </summary>
    /// <typeparam name="T"> T can be Ugyfel, DVDkolcsozo, Kolcsonzes, Film</typeparam>
    public class Logic<T> : ILogic<T>
        where T : class
    {
        private readonly IRepository<T> repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="Logic{T}"/> class.
        /// </summary>
        /// <param name="repo"> Dep. injection</param>
        public Logic(IRepository<T> repo)
        {
        this.repo = repo;
        }

        /// <summary>
        /// It transfers the data from the program.cs to the Delete method in the Repository.cs, and gives back the returned parameter from the Repository.cs
        /// </summary>
        /// <param name="obj">This method brings the object type to the Repository.cs</param>
        /// <returns> It returns with a boolean, to check if the Delte Method was successful </returns>
        public bool Delete(T obj)
        {
            try
            {
                this.repo.Delete(obj);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// This is the GetAll method which return an entity from the database
        /// </summary>
        /// <returns>It returns an entity from the database which can be Film,Ugyfel,Kolcsonzes,DVDkolcsonzo</returns>
        public IEnumerable<T> GetAll()
        {
            return this.repo.GetAll();
        }

        /// <summary>
        /// Noncrud
        /// </summary>
        /// <returns>returns ugyfels who are under 18 or even</returns>
        public IEnumerable<Ugyfel> GetUgyfelsWhoUnderOrEqual18()
        {
            var not18 = from i in this.repo.GetAll() as IEnumerable<Ugyfel>
                        where i.Eletkor <= 18
                        select new Ugyfel
                        {
                            Nev = i.Nev,
                            Eletkor = i.Eletkor
                        };
            return not18;
        }

        /// <summary>
        /// Noncrud
        /// </summary>
        /// <returns> returns the nolan producered films</returns>
        public IEnumerable<Film> GetChristopherNolanFilms()
        {
            var nolan = from i in this.repo.GetAll() as IEnumerable<Film>
                        where i.Rendezo == "Christopher Nolan"
                        select new Film
                        {
                            Cim = i.Cim
                        };

            return nolan;
        }

        /// <summary>
        /// Noncrud
        /// </summary>
        /// <returns>returns ugyfels who are living in budapest</returns>
        public IEnumerable<Ugyfel> GetUgyfelWhoLivesInBudapest()
        {
            var bp = from i in this.repo.GetAll() as IEnumerable<Ugyfel>
                     where i.Cim.ToLower().Contains("budapest")
                     select new Ugyfel
                     {
                         Nev = i.Nev
                     };

            return bp;
        }

        /// <summary>
        /// It transfers the data from the program.cs to the Insert method in the Repository.cs, and gives back the returned parameter from the Repository.cs
        /// </summary>
        /// <param name="obj">This method brings the object type to the Repository.cs</param>
        /// <returns> It returns with a boolean, to check if the Insert Method was successful </returns>
        public bool Insert(T obj)
        {
            try
            {
                this.repo.Insert(obj);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// It transfers the data from the program.cs to the Update method in the Repository.cs, and gives back the returned parameter from the Repository.cs
        /// </summary>
        /// <param name="obj">This method brings the object type to the Repository.cs</param>
        /// <returns> It returns with a boolean, to check if the Update Method was successful </returns>
        public bool Update(T obj)
        {
            try
            {
                this.repo.Update(obj);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// JAVA
        /// </summary>
        /// <returns> JAVA XML</returns>
        public string JavaEndPoint()
        {
            XDocument d = XDocument.Load("http://localhost:8080/Java/FilmsQuery?kolcsonzo=remenyrabjai");

            string downloads = d.ToString();

            return downloads;
        }

        public T GetOne(int id)
        {
            return this.repo.GetOne(id);
        }
    }

    public class UgyfelLogic : IUgyfelLogic
    {
        IUgyfelRepository ugyfelRepo;

        public UgyfelLogic(IUgyfelRepository ugyfelRepo)
        {
            this.ugyfelRepo = ugyfelRepo;
        }

        public bool ChangeUgyfel(int id, string name, string email, string address, int age, string mobile)
        {
            var ugyfel = this.ugyfelRepo.GetAll().SingleOrDefault(x => x.Nev == name);
            return this.ugyfelRepo.ChangeUgyfel(id, name, email, address, age, mobile);
        }

        public Ugyfel GetOneUgyfel(int id)
        {
            return this.ugyfelRepo.GetOne(id);
        }

        public void AddUgyfel(string name, string address, string email, int age, string mobile)
        {
            var ugyfel = this.ugyfelRepo.GetAll().SingleOrDefault(x => x.Nev == name);
            Ugyfel ujugyfel = new Ugyfel();
            ujugyfel.Nev = name;
            ujugyfel.Cim = address;
            ujugyfel.E_mail = email;
            ujugyfel.Eletkor = age;
            ujugyfel.Telefon = mobile;
            this.ugyfelRepo.Insert(ujugyfel);
        }

        public bool Remove(int id)
        {
            return this.ugyfelRepo.Remove(id);
        }

        public IList<Ugyfel> GetAllUgyfel()
        {
            return this.ugyfelRepo.GetAll().ToList();
        }
    }
}
