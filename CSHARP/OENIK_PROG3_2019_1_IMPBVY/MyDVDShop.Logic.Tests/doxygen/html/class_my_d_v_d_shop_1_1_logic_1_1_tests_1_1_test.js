var class_my_d_v_d_shop_1_1_logic_1_1_tests_1_1_test =
[
    [ "GivenDVDkolcsonzo_WhenDelete_ReturnsTrue", "class_my_d_v_d_shop_1_1_logic_1_1_tests_1_1_test.html#a258ddd1760aa811c8411915de677a4ea", null ],
    [ "GivenDVDkolcsonzo_WhenUpdate_ReturnsTrue", "class_my_d_v_d_shop_1_1_logic_1_1_tests_1_1_test.html#afdd19072e298b20d1de0ed879b3306c3", null ],
    [ "GivenFilm_WhenDelete_ReturnsTrue", "class_my_d_v_d_shop_1_1_logic_1_1_tests_1_1_test.html#a69c79ef5503eaa4db8b4adc2a0f9970d", null ],
    [ "GivenFilm_WhenUpdate_ReturnsTrue", "class_my_d_v_d_shop_1_1_logic_1_1_tests_1_1_test.html#adf59639c286357499c191d2037dacc6e", null ],
    [ "GivenFilms_WhenGetFilmsMadebyChristopherNolan", "class_my_d_v_d_shop_1_1_logic_1_1_tests_1_1_test.html#ac9bf84f2dbd0e57850d23118f0e28446", null ],
    [ "GivenKolcsonzes_WhenDelete_ReturnsTrue", "class_my_d_v_d_shop_1_1_logic_1_1_tests_1_1_test.html#a2ea08e8a65dc5f6d8fabdb0ebdebb979", null ],
    [ "GivenKolcsonzes_WhenUpdate_ReturnsTrue", "class_my_d_v_d_shop_1_1_logic_1_1_tests_1_1_test.html#adebbd6fd6189328addaa8f66387905ab", null ],
    [ "GivennewDVDkolcsonzo_WhenInsert_ReturnsTrue", "class_my_d_v_d_shop_1_1_logic_1_1_tests_1_1_test.html#a090658a53c667125582b5be77b2ff832", null ],
    [ "GivennewFilm_WhenInsert_ReturnsTrue", "class_my_d_v_d_shop_1_1_logic_1_1_tests_1_1_test.html#a80486ae20703a24ea7f9ec610f67372b", null ],
    [ "GivennewKolcsonzes_WhenInsert_ReturnsTrue", "class_my_d_v_d_shop_1_1_logic_1_1_tests_1_1_test.html#ab97c7c107e7d3a80507a9226076428e8", null ],
    [ "GivennewUgyfel_WhenInsert_ReturnsTrue", "class_my_d_v_d_shop_1_1_logic_1_1_tests_1_1_test.html#acd26e6fb10e6ef3aa8d95ac651b96914", null ],
    [ "GivenUgyfel_WhenDelete_ReturnsTrue", "class_my_d_v_d_shop_1_1_logic_1_1_tests_1_1_test.html#a1bdb5013faa7291f16fe49ec5b141637", null ],
    [ "GivenUgyfel_WhenUpdate_ReturnsTrue", "class_my_d_v_d_shop_1_1_logic_1_1_tests_1_1_test.html#ae4438c4219c2c04e856d90961688f905", null ],
    [ "GivenUgyfels_WhenGetUgyfelsWhoAreLivingInBudapest", "class_my_d_v_d_shop_1_1_logic_1_1_tests_1_1_test.html#a28994946d62e089519ccde7915535b39", null ],
    [ "GivenUgyfels_WhoAreUnderorEven18", "class_my_d_v_d_shop_1_1_logic_1_1_tests_1_1_test.html#aa988944891abe1b73dfed4c2df7d70da", null ],
    [ "Setup", "class_my_d_v_d_shop_1_1_logic_1_1_tests_1_1_test.html#a3364b7de2d2d7a80f60adb1558e962a9", null ],
    [ "WhenGetAllDVDkolcsonzo_ThenReturnsList", "class_my_d_v_d_shop_1_1_logic_1_1_tests_1_1_test.html#aff81aedafc2d0c02d4d1fc675be957c9", null ],
    [ "WhenGetAllFilm_ThenReturnsList", "class_my_d_v_d_shop_1_1_logic_1_1_tests_1_1_test.html#a2201b89215a18c8d3a79afd63e8cfb3a", null ],
    [ "WhenGetAllKolcsonzes_ThenReturnList", "class_my_d_v_d_shop_1_1_logic_1_1_tests_1_1_test.html#a329bf1c38a091dc3267da3239d42d23d", null ],
    [ "WhenGetAllUgyfel_ThenReturnsList", "class_my_d_v_d_shop_1_1_logic_1_1_tests_1_1_test.html#aea4973ec0bb4f1ab7363d8552eb02c6b", null ]
];