package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;



@XmlRootElement(name ="film")
@XmlAccessorType(XmlAccessType.FIELD)
public class Film implements Serializable{
    
    private static int nextid;
    private static int nextdvdkolcsonzoid;
    
    @XmlElement
    private int id;
    @XmlElement
    private int dvdkolcsonzoid;
    @XmlElement
    private String cim;
    @XmlElement
    private String rendezo;
    @XmlElement
    private String filmstudio;
    @XmlElement
    private String megjelendesideje;
    @XmlElement
    private int bevetel;
    @XmlElement
   private List<Film> films = new ArrayList<>();

    public Film() {
       this.id = nextid++;
       this.dvdkolcsonzoid = nextdvdkolcsonzoid++;
    }

    
    public int getId() {
        return id;
    }


    public int getDvdkolcsonzoid() {
        return dvdkolcsonzoid;
    }


    public String getCim() {
        return cim;
    }

    public void setCim(String cim) {
        this.cim = cim;
    }

    public String getRendezo() {
        return rendezo;
    }

    public void setRendezo(String rendezo) {
        this.rendezo = rendezo;
    }

    public String getFilmstudio() {
        return filmstudio;
    }

    public void setFilmstudio(String filmstudio) {
        this.filmstudio = filmstudio;
    }

    public String getMegjelendesideje() {
        return megjelendesideje;
    }

    public void setMegjelendesideje(String megjelendesideje) {
        this.megjelendesideje = megjelendesideje;
    }

    public int getBevetel() {
        return bevetel;
    }

    public void setBevetel(int bevetel) {
        this.bevetel = bevetel;
    }

    public List<Film> getFilms() {
        return films;
    }
    
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Film other = (Film) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }
    
    
    
    
    
}
